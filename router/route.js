const express = require('express');
const router = express.Router();

const controller = require('../controller/logic');

// Home Page
router.get('/', controller.homePage);

// GET API for all contacts
router.get('/contacts', controller.getAllContacts);

// GET API for all companies
router.get('/companies', controller.getAllCompanies);

// GET API for all Deals
router.get('/deals', controller.getAllDeals);

module.exports = router;