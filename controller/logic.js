require('dotenv').config();
const axios = require('axios');

const baseUri = "https://api.hubapi.com/crm/v3/objects";
const API_KEY = process.env.API_KEY;

// Home Page Hello
const homePage = async (req, res) =>{
    res.send('Hello');
};

//GET API for all Contacts
const getAllContacts = async (req, res) => {
    const getContactsUri = `${baseUri}/contacts?hapikey=${API_KEY}`;
    try {
        const resp = await axios.get(getContactsUri);
        const data = resp.data.results;
        // res.json(data);
        res.render('contacts', {data});
    } catch (error) {
        console.log(error);
    }
};

//GET API for all Companies List
const getAllCompanies = async (req, res) => {
    const getCompaniesUri = `${baseUri}/companies?hapikey=${API_KEY}`;
    try {
        const resp = await axios.get(getCompaniesUri);
        const data = resp.data.results;
        // res.json(data);
        res.render('companies',{data});
    } catch (error) {
        console.log(error);
    }
};

//GET API for all Deals
const getAllDeals = async (req,res) => {
    const getDealsUri = `${baseUri}/deals?hapikey=${API_KEY}`;
    try {
        const resp = await axios.get(getDealsUri);
        const data = resp.data.results;
        // res.json(data);
        res.render('deals',{data});
    } catch (error) {
        console.log(error)
    }
}

// export controller functions
module.exports = {homePage, getAllContacts, getAllCompanies, getAllDeals};