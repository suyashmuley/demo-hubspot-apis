const express = require('express');
const PORT = 5000;
const routes = require('./router/route');

const app = express();

app.set('view engine','pug');

app.use(express.urlencoded({ extended: true}));
app.use(express.json());

app.use('/', routes);

app.listen(PORT, ()=> console.log(`Listening on http://localhost:${PORT}`));
